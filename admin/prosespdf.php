<?php

header('Content-Type: text/html; charset=UTF-8');

include 'vendor/autoload.php';

include 'Classes/stem_indonesia.php';

include "../koneksi/DB_Connect.php";
$db = new DB_Connect();
$db->connect();

$message = '';
$texts   = array();
 	$term = array();
$headInsertTEMP = "insert into pdf(nama,detil) values ";
$awal = microtime(true);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	try {
        $content = '';

		
		if (isset($_POST['inputUrl']) && preg_match('/^https?:\/\//', trim($_POST['inputUrl']))) {
			$content = file_get_contents(trim($_POST['inputUrl']));
		} elseif (isset($_FILES['inputFile']) && $_FILES['inputFile']['type'] == 'application/pdf') {
			$content = file_get_contents($_FILES['inputFile']['tmp_name']);
		}
		
		if ($content) {
			$parser = new \Smalot\PdfParser\Parser();
			$pdf    = $parser->parseContent($content);
			$pages  = $pdf->getPages();
			
			foreach ($pages as $page) {
				$texts[] = $page->getText();
			}
		} else {
			throw new Exception('Unable to retrieve content. Check if it is really a pdf file.');
		}
	} catch(Exception $e) {
		$message = $e->getMessage();
	}
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Proses - PDF</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container">
		<h1><a href="/">PDF Parser - Demo sample page</a></h1>
		
		<?php if ($message): ?>
			<div class="alert alert-danger"><?php echo $message; ?></div>
		<?php endif; ?>
		
		<form class="form-horizontal" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="inputUrl" class="col-lg-2 control-label">Url</label>
				<div class="col-lg-10">
					<input type="url" class="form-control" id="inputUrl" name="inputUrl" placeholder="Url">
				</div>
			</div>
			<div class="form-group">
				<label for="inputFile" class="col-lg-2 control-label">File</label>
				<div class="col-lg-10">
					<input type="file" class="form-control" id="inputFile" name="inputFile" placeholder="File">
				</div>
			</div>
		    <div class="form-group">
				<div class="col-lg-offset-2 col-lg-10">
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>

<?php
    $astoplist = array ("yang", "juga", "dari", "dia", "kami", "kamu", "ini", "itu", 
                               "atau", "dan", "tersebut", "pada", "dengan", "adalah", "yaitu", "dalam", "di"); 
	
 	//$pdf1 = array();
 	//$pdf2 = array();
 $idx = 1;	
 $idx2 = 1;
 $insertTEMP = "";
 $jumlahKata = 0;
 $jumlahHalaman = sizeof($texts);
 
 $myFile = "data.txt";
 $fh = fopen($myFile, 'w') or die("can't open file");

 
for ($x=0; $x<sizeof($texts); $x++){
//hilangkan tanda baca cth: , . " ; _ - ' ! ? { }  atau selain huruf , ",", "." , "!", "?" , ":" , ";"
$texts[$x] = str_replace(",", "", $texts[$x] );
$texts[$x]  = str_replace(".", "", $texts[$x] );
$texts[$x]  = str_replace(";", "", $texts[$x] ); 
$texts[$x]  = str_replace("(", "", $texts[$x] ); 
$texts[$x]  = str_replace(")", "", $texts[$x] ); 
$texts[$x]  = str_replace(" : ", " ", $texts[$x] );
$texts[$x]  = str_replace(":", "", $texts[$x] );
$texts[$x]  = str_replace("…", "", $texts[$x] );
//$texts[$x]  = str_replace("-", "", $texts[$x] );
$texts[$x]  = str_replace("?", "", $texts[$x] );
$texts[$x]  = str_replace("\"", "", $texts[$x] );
$texts[$x]  = str_replace("+", "", $texts[$x] );
$texts[$x]  = str_replace("=", "", $texts[$x] );
$texts[$x]  = str_replace("%", "", $texts[$x] );
$texts[$x] = str_replace("\n", " ", $texts[$x] );
$texts[$x] = str_replace("***", "",$texts[$x] );
$texts[$x] = str_replace("‘", "",$texts[$x]);
$texts[$x] = str_replace("’", "", $texts[$x]);
$texts[$x] = str_replace("”", "", $texts[$x]);
$texts[$x] = str_replace("“","", $texts[$x]);
$texts[$x] = str_replace("!","", $texts[$x]);
//$texts[$x] = str_replace("www", "", $texts[$x]);
//$texts[$x] = str_replace("wwwrajaebookgratiscom", "", $textx[$x]);

//membuat lowercase (huruf kecil semua)
$texts[$x] = strtolower($texts[$x]);
//echo $texts[$x];
//$term[$x]= explode(" " ,$texts[$x]);
$terms= explode(" ",$texts[$x]);
$k = 1;

$stringData = "";
$j = 0;
foreach ($terms as $t) {
if(strlen($t) > 2){
//for ($j=0; $j<sizeof($terms); $j++){
	//if (($term[$x][$j] != "") && ($term[$x][$j] != "\n")){
//	if (strlen($terms[$j]) > 2 ){
		//$pdf1[$jumlahKata] = $term[$x][$j];
		$jumlahKata += 1;
		//"(" . $x . "," . $j . ")"
		$stringData .= $jumlahKata . " : " . ltrim(rtrim($t)) . " : " . "1" . "\n";
		//fwrite($fh, $stringData);
		
		
		
		//$nama = $term[$x][$j];
		//$insertTEMP .= "('$nama','1'), ";
		
		//if($jumlahKata == 2000 * $idx1){
		//	$insertTEMP = rtrim($insertTEMP, ", ");
		//	$insertTEMP .= ";";
		//	$insertTEMP .= $headInsertTEMP;
		//	$idx = $idx + 1;
		//}
 			
	//$insertPDFtempterm = mysql_query("insert into pdf(nama,detil) values('$nama','1')") or die (mysql_error());
			//$k = $k + 1;  	
			//
	}
	$j=$j+1;
}
fwrite($fh, $stringData);

unset($terms);
$clearPdf = mysql_query("truncate table pdf") or die (mysql_error());
}

$content = file_get_contents("data.txt");
$lines = explode("\n", $content);
foreach ($lines as $line) {
    $row = explode(":", $line);
    $query = "INSERT INTO pdf SET id = '" . trim($row[0]) . "', nama = '" . trim($row[1]) . "', detil = '" . trim($row[2]) . "'";
//    $query = "INSERT INTO pdf SET nama = '" . trim($line) . "'";
    mysql_query($query);
}
echo "jumlah Kata " . $jumlahKata; 
echo " jumlah Stemming " . $idx2; 
echo " jumlah Halaman " . $jumlahHalaman; 
echo "<br>";

 
$idx = 0;
//remove stopwords
 	$queryTemp = "select * from pdf where nama not in (select kata from stopwords)";
 	
 	$isiTemp = mysql_query($queryTemp) or die (mysql_error());
 	if (mysql_num_rows($isiTemp) > 0)
 	{
 		$terms = array();
 		$idx2 = 1;
 		//stemming porter bahasa
 		while($row = mysql_fetch_array($isiTemp) ){
 			$dt = $row['nama'];
 			
			if (strlen($dt) > 2 ){
				//echo  $idx2 . " " . $dt . " :";
 				$terms[$idx] = $dt;
 				$idx = $idx + 1;
 				//stemming porter bahasa indonesia
 				$dtstm = stemmingPorterStep1($dt);
 				//echo " " . $dtstm;
 				$dtstm = stemmingPorterStep2($dtstm);
 				// 				echo " " . $dtstm;
 				$dtstm = stemmingPorterStep3($dtstm);
 				//				echo " " . $dtstm;
 				$dtstm = stemmingPorterStep4($dtstm);
 				//				echo " " . $dtstm;
 				$dtstm = stemmingPorterStep5($dtstm);
 				//echo " " . $dtstm . "<br>";
 				//echo $dtstm . "|";
 				$insertStem = mysql_query("INSERT INTO pdf SET  nama = '" . $dtstm . "', detil = '2'");
 						//$insertPDFtempstem = mysql_query($headInsertTEMP . $insertTEMP) or die (mysql_error());
 						$idx2 = $idx2 +1;

 					//$insertPDFtempstem = mysql_query("insert into pdf(nama,detil) values('$dtstm','2')") or die (mysql_error());
 				}
 			}
 		} else {echo "query gagal";}
 		
$akhir = microtime(true);
$lama = $awal - $akhir;
echo " waktu proses: " . $lama; 	
//insert term ke db
 	$ambilStemming = mysql_query("insert into terms (term,jumlah) SELECT nama term, COUNT( * ) jumlah FROM pdf WHERE detil = 2 AND nama NOT IN ('') GROUP BY nama") or die (mysql_error());
 	
 	//$idx = 0;
 	//if (mysql_num_rows($ambilStemming) > 0)
 	//{
 	//	while($row = mysql_fetch_array($ambilStemming) ){
 	//		$nm = $row['nama'];
 	//		$j = $row['jml'];
	//		$insertT = mysql_query("INSERT INTO terms SET  term = '" . $nm . "', jumlah = '$j'");
 	//	}
 	//}
 	
//$insertPDF = mysql_query("LOAD DATA INFILE './data.txt' INTO TABLE pdf FIELDS TERMINATED BY ' : '  LINES TERMINATED BY '\n'")  or die (mysql_error());
//if(mysql_num_rows($insertPDF)>0){
//	unset($lines);
//	$fh = fopen( 'data.txt', 'w' );
//	fclose($fh);
//}  
//$insertTEMP = rtrim($insertTEMP, ", ");
//$insertTEMP .= ";";

//if($insertTEMP != ";"){
//echo $headInsertTEMP . $insertTEMP;
//$insertPDFtempstem = mysql_query($headInsertTEMP . $insertTEMP) or die (mysql_error());
//}

//destroy array
//unset($texts);
//unset($term);



//step2 erase stopwords
//$queryTemp = "select kata from stopwords";
// 	$isiTemp = mysql_query($queryTemp) or die (mysql_error());
// 	if (mysql_num_rows($isiTemp) > 0)
// 	{
// 		while($row = mysql_fetch_array($isiTemp) ){
// 			$dt = $row['kata'];
// 				for($x=0;$x<$jumlahHalaman;$x++) {
// 					for($y=0;$y<sizeof($term[$x]);$y++) {
// 						if($term[$x][$y] == $dt){
// 							$term[$x][$y] = "";
// 							echo "(" . $x . "," . $y . ")";
// 						} 
// 					}
// 				}
// 			}
// 	}		


   
//step3 porter
//for($x=0;$x<$jumlahHalaman;$x++) {
// 	for($y=0;$y<sizeof($term[$x]);$y++) {
 						//if(str_word_count($term[$x][$y]) == 1) {
//						echo "(" . $x . "," . $y . ")";
 						//if(($term[$x][$y] != "") ){
 						//	$satukata = $term[$x][$y];
 						//	
 						//	$dtstm = stemmingPorterStep1($satukata);
 						//	$dtstm = stemmingPorterStep2($dtstm);
			 			//	$dtstm = stemmingPorterStep3($dtstm);
 						//	$dtstm = stemmingPorterStep4($dtstm);
 						//	$dtstm = stemmingPorterStep5($dtstm);
 						//	$term[$x][$y] = $dtstm;
 							

 							//echo $idx2 . " " . $dtstm . " <br>";
 							
 						//	$insertTEMP .= "('$dtstm','1'), ";
 							
 						//	$idx2 = $idx2 + 1;
 							
						//}
					
// 	}
 	
 	//$insertTEMP = rtrim($insertTEMP, ", ");
	//$insertTEMP .= ";";
//	echo "<br>";
	//echo  $insertTEMP;
	
	//$insertPDFtempstem = mysql_query($headInsertTEMP . $insertTEMP) or die (mysql_error());
//}



?>

		
		
		
		<p class="text-center"><a href="http://www.pdfparser.org" target="_blank">PDF Parser</a> is a <a href="http://www.php.net" target="_blank">PHP</a> provided by <a href="https://twitter.com/sebastienmalot" target="_blank">@sebastienmalot</a>.<br/>
		Code licenced under <a href="http://www.pdfparser.org/license" target="_blank">GPLv2 license</a>.
		</p>
		
		<ul class="list-inline text-center">
			<li><a href="http://www.pdfparser.org/documentation" target="_blank">Documentation</a></li>
			<li><a href="https://github.com/smalot/pdfparser" target="_blank">GitHub</a></li>
			<li><a href="https://packagist.org/packages/smalot/pdfparser" target="_blank">Packagist</a></li>
			<li><a href="http://www.pdfparser.org/contact" target="_blank">Contact</a></li>
		</ul>
		
	</div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>