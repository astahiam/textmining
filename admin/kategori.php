<?php 

include "koneksi/DB_Connect.php";
$db = new DB_Connect();
$db->connect();

include('header.php'); 
include('menuatas.php');
?>
<!-- CONTENT START -->
    <div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
    <h1 class="dashboard">Kategori Buku</h1>
    </div>
    
    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">
    <!-- FIRST SORTABLE COLUMN START -->
      <div class="column" id="left">
      
        <div class="portlet">
		<div class="portlet-header">Kategori</div>

		<div class="portlet-content">
		  <p>Untuk menambah kategori buku ke database.</p>
		  <h3></h3>
		  <form id="form1" name="form1" method="post" action="savekategori.php" enctype="multipart/form-data">
		    <label>Nama Kategori</label>
		     <input type="text" name="nama" id="nama" class="largeInput"  />
			<label>Parent</label>
		    <select name="parent" id="parent">
		    <option value="0"></option>
		    <?php
		    $queryKat = "select idkategori,nama from kategori";
              
              $q = mysql_query($queryKat) or die (mysql_error());
			    
				if(mysql_num_rows($q) > 0){
				
				while($ftch = mysql_fetch_array($q)){
			
						
					echo "<option value=" . $ftch['idkategori'] ." >";
					echo $ftch['nama'] . "</option>";
						}
				}
			?>
			</select>					
            <input type="submit" value="Tambah">
		  </form>
		  <p>&nbsp;</p>
		</div>
        </div>
      </div>
      <!-- FIRST SORTABLE COLUMN END -->
      <!-- SECOND SORTABLE COLUMN START -->
      <div class="column">
      
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
        <div class="portlet-header fixed"><img src="images/icons/user.gif" width="16" height="16" alt="Tabel Buku" /> Tabel Kategori</div>
		<div class="portlet-content nopadding">
        <form action="" method="post">
          <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="kategory">
            <thead>
              <tr>
                <th width="136" scope="col">ID</th>
                <th width="102" scope="col">Nama</th>
                <th width="109" scope="col">Parent</th>
                <th width="90" scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $querybuku = "select * from kategori";
              
              $q = mysql_query($querybuku) or die (mysql_error());
			  $id = "";
			  $nama = "";
			  $parent = "";
			  		  
				if(mysql_num_rows($q) > 0){
					while($ftch = mysql_fetch_array($q)){
					$id=$ftch['idkategori'];
					$nama = $ftch['nama'];
					$parent = $ftch['idparent'];
			  		echo "<tr>";
			  		echo "<td>" . $id . "</td>";
			  		echo "<td>" . $nama . "</td>";
			  		echo "<td>" . $parent . "</td>";
			  		
			  		?>
			  		<td></td>
			  		<td width="90"><a href="#" class="edit_icon" title="Edit"></a> </td>
              		</tr>
			  		<?php
					}
				}
				?>
              <tr>
                
                
                
              <tr class="footer">
                <td colspan="3"></td>
                <td align="right">&nbsp;</td>
                <td colspan="3" align="right">
				<!--  PAGINATION START  -->             
                    <div class="pagination">
                    <span class="previous-off">&laquo; Previous</span>
                    <span class="active">1</span>
                    <a href="query_41878854">2</a>
                    <a href="query_8A8058C2">3</a>
                    <a href="query_2823E521">4</a>
                    <a href="query_B322F5B7">5</a>
                    <a href="query_3A2A444D">6</a>
                    <a href="query_912D14DB">7</a>
                    <a href="query_41878854" class="next">Next &raquo;</a>
                    </div>  
                <!--  PAGINATION END  -->       
                </td>
              </tr>
            </tbody>
          </table>
        </form>
		</div>
      </div>
<!--  END #PORTLETS -->  
   </div>
    <div class="clear"> </div>
<!-- END CONTENT-->    
<?php include('footer.php');?>