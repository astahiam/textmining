<?php
include_once "koneksi/DB_Connect.php";
include 'Classes/stem_indonesia.php';

class prosesindexing {
	private $halamanKembali;

	function __construct($halaman){
		$db = new DB_Connect();
		$db->connect();
		$this->halamanKembali = $halaman;
	}
	
	public function getHalamanKembali()
	{
		return $this->halamanKembali;
	}
	public function setHalamanKembali($halaman)
	{
		$this->halamanKembali = $halaman;
	}

	function FilteringTokenizing($urlbuku){
		$awal = microtime(true);
		//baca file pdf
		try {
			if (isset($urlbuku) && preg_match('/^https?:\/\//', trim($urlbuku))) {
				$content = file_get_contents(trim($urlbuku));
			} 
		
			if ($content) {
				$parser = new \Smalot\PdfParser\Parser();
				$pdf    = $parser->parseContent($content);
				$pages  = $pdf->getPages();
			
				foreach ($pages as $page) {
					$texts[] = $page->getText();
				}
			} else {
				throw new Exception('Unable to retrieve content. Check if it is really a pdf file.');
			}
			} catch(Exception $e) {
				$message = $e->getMessage();
			}
		//proses tokenizing			
 		$idx = 1;	
 		$idx2 = 1;
 		$insertTEMP = "";
 		$jumlahKata = 0;
 		$jumlahHalaman = sizeof($texts);

		$headInsert = "INSERT INTO PDF (nama, detil) VALUES ";
		$tempInsert = "";
		for ($x=0; $x<sizeof($texts); $x++){
			//hilangkan tanda baca cth: , . " ; _ - ' ! ? { }  atau selain huruf , ",", "." , "!", "?" , ":" , ";"
			$texts[$x] = str_replace(",", "", $texts[$x] );
			$texts[$x]  = str_replace(".", "", $texts[$x] );
			$texts[$x]  = str_replace(";", "", $texts[$x] ); 
			$texts[$x]  = str_replace("(", "", $texts[$x] ); 
			$texts[$x]  = str_replace(")", "", $texts[$x] ); 
			$texts[$x]  = str_replace(" : ", " ", $texts[$x] );
			$texts[$x]  = str_replace(":", "", $texts[$x] );
			$texts[$x]  = str_replace("…", "", $texts[$x] );
			//$texts[$x]  = str_replace("-", "", $texts[$x] );
			$texts[$x]  = str_replace("?", "", $texts[$x] );
			$texts[$x]  = str_replace("\"", "", $texts[$x] );
			$texts[$x]  = str_replace("+", "", $texts[$x] );
			$texts[$x]  = str_replace("=", "", $texts[$x] );
			$texts[$x]  = str_replace("%", "", $texts[$x] );
			$texts[$x] = str_replace("\n", " ", $texts[$x] );
			$texts[$x] = str_replace("***", "",$texts[$x] );
			$texts[$x] = str_replace("‘", "",$texts[$x]);
			$texts[$x] = str_replace("’", "", $texts[$x]);
			$texts[$x] = str_replace("”", "", $texts[$x]);
			$texts[$x] = str_replace("“","", $texts[$x]);
			$texts[$x] = str_replace("!","", $texts[$x]);
			$texts[$x] = str_replace("'","", $texts[$x]);

			//membuat lowercase (huruf kecil semua)
			$texts[$x] = strtolower($texts[$x]);
			//token terms
			$terms= explode(" ",$texts[$x]);
			$k = 1;

			$stringData = "";
			$tempInsert = "";
			$j = 0;
			foreach ($terms as $t) {
				if(strlen($t) > 2){
					$jumlahKata += 1;
					$stringData .= $jumlahKata . " : " . ltrim(rtrim($t)) . " : " . "1" . "\n";
					$tempInsert .= "('" . ltrim(rtrim($t)) . "','1'),";
					}
				$j=$j+1;
			}
			$tempInsert = substr($tempInsert,0,-1);
			$tempInsert .= ";";
	
			$query = $headInsert . $tempInsert;
	
			mysql_query($query) or die (mysql_error());
	
			unset($terms);
		}

		echo "jumlah Kata " . $jumlahKata; 
		echo " jumlah Halaman " . $jumlahHalaman; 
		echo "<br>";

		$idx = 0;
		//filtering
		$stopwordsremover = "delete from pdf where nama in (select kata from stopwords)";
		$jumlahterhapus = mysql_query($stopwordsremover) or die (mysql_error());
		//timer
		$akhir = microtime(true);
		$lama = $akhir - $awal ;
		echo " waktu proses: " . $lama; 
		?>
		<script>
		alert('<?php echo "jumlah Kata " . $jumlahKata . " jumlah Halaman " . $jumlahHalaman . " waktu proses: " . $lama;  ?>');
		window.location.href = '<?php echo getHalamanKembali();?>';
		</script>
		<?php
	}
	
	function Stemming(){
		$awal = microtime(true);
		//proses stemming porter bahasa
		try {	
			$queryTemp = mysql_query("select * from pdf") or die (mysql_error());
			$jumlahKata = mysql_num_rows($queryTemp);
			echo $jumlahKata;
			if (mysql_num_rows($queryTemp) > 0)
			{
				$terms = array();
				$idx2 = 1;
				//stemming porter bahasa
				while($row = mysql_fetch_array($queryTemp) ){
					$dt = $row['nama'];			
					if (strlen($dt) > 2 ){
						//echo  $idx2 . " " . $dt . " :";
						$terms[$idx] = $dt;
						$idx = $idx + 1;
						//stemming porter bahasa indonesia
						$dtstm = stemmingPorterStep1($dt);
						//echo " " . $dtstm;
						$dtstm = stemmingPorterStep2($dtstm);
						// 				echo " " . $dtstm;
						$dtstm = stemmingPorterStep3($dtstm);
						//				echo " " . $dtstm;
						$dtstm = stemmingPorterStep4($dtstm);
						//				echo " " . $dtstm;
						$dtstm = stemmingPorterStep5($dtstm);
						//echo " " . $dtstm . "<br>";
						//echo $dtstm . "|";
						$insertStem = mysql_query("INSERT INTO pdf SET  nama = '" . $dtstm . "', detil = '2'");
						$idx2 = $idx2 +1;
						}
					}
				} else {echo "query gagal";}
				$queryTemp = mysql_query("select * from pdf where detil = 2") or die (mysql_error());
				$jumlahKata = mysql_num_rows($queryTemp);
			} catch(Exception $e) {
				$message = $e->getMessage();
			}	
		//timer	
		$akhir = microtime(true);
		$lama = $akhir - $awal;
		echo " waktu proses: " . $lama; 
		?>
		<script>
		alert('<?php echo "jumlah Kata " . $jumlahKata . " jumlah Stemming " . $idx2 . " waktu proses: " . $lama;  ?>');
		window.location.href = '<?php echo getHalamanKembali();?>';
		</script><?php
	}
	
	function TermIndexing($idbuku){
		$awal = microtime(true);
		//proses term indexing
		try {
			$queryTemp = mysql_query("SELECT nama term, COUNT( * ) jumlah FROM pdf WHERE detil = 2 AND nama NOT IN ('') GROUP BY nama") or die (mysql_error());
			$jumlahKata = mysql_num_rows($queryTemp);
			echo $jumlahKata;
			if (mysql_num_rows($queryTemp) > 0)
			{
		
				$idx2 = 0;
				//stemming porter bahasa
				while($row = mysql_fetch_array($queryTemp) ){
					$dt = $row['term'];
					$j = $row['jumlah'];
					$cek = "select idterms, term, jumlah from terms where term = '$dt'";
					$Qcek = mysql_query($cek) or die (mysql_error());
					if(mysql_num_rows($Qcek) > 0){
						while($ftch = mysql_fetch_array($Qcek)){
							$jml = $ftch['jumlah'];
							$idt = $ftch['idterms'];
							$jumlah = $jml + $j;
							$idx2 = $idx2 + 1;
						 $Qupdate = mysql_query("UPDATE terms set jumlah = $jumlah where term = '$dt'") or die (mysql_error());
						 $insertEbookTerm = mysql_query("INSERT INTO ebookterm set idbuku = $idbuku, idterms = $idt , jumlah = $j ") or die (mysql_error());
								}
						}
					else {
						$Qinsert = mysql_query("INSERT INTO terms set term = '$dt' , jumlah = '$j' ") or die (mysql_error());
						$insertEbookTerm = mysql_query("INSERT INTO ebookterm set idbuku = $idbuku, idterms = " . mysql_insert_id() . " , jumlah = '$j' ") or die (mysql_error());
						}
					}
				} else {echo "query gagal";}		
	
			} catch(Exception $e) {
				$message = $e->getMessage();
			}
	
		//timer
		$akhir = microtime(true);
		$lama = $akhir - $awal;
		echo " waktu proses: " . $lama; 
		?>
		<script>
		alert('<?php echo "jumlah Kata " . $jumlahKata . " jumlah  " . $idx2 . " waktu proses: " . $lama;  ?>');
		window.location.href = '<?php echo getHalamanKembali();?>';
		</script><?php
	}
}
?>