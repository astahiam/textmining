<?php 

include "koneksi/DB_Connect.php";
$db = new DB_Connect();
$db->connect();

include('header.php'); 
include('menuatas.php');
?>
<!-- CONTENT START -->
    <div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
    <h1 class="dashboard">Import Buku</h1>
    </div>
    
    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">
    <!-- FIRST SORTABLE COLUMN START -->
      <div class="column" id="left">
      
        <div class="portlet">
		<div class="portlet-header">Insert Buku</div>

		<div class="portlet-content">
		  <p>Untuk menambah buku ke database.</p>
		  <h3></h3>
		  <form id="form1" name="form1" method="post" action="savebuku.php" enctype="multipart/form-data">
		    <label>Judul</label>
		     <input type="text" name="judul" id="judul" class="largeInput"  />
			<label>Tahun</label>
            <input type="text" name="tahun" id="tahun" class="smallInput"/>
            <label>Pengarang</label>
            <input type="text" name="pengarang" id="pengarang" class="smallInput"/>
            <label>Sinopsis</label>
		    <textarea name="sinopsis" cols="45" rows="3" class="smallInput" id="textarea"></textarea>
		    <label for="filecover">cover</label>
		    <input type="file" name="filecover" id="filecover">
		    <label>kategori</label>
		    
		    <?php
		    $queryKat = "select idkategori,nama from kategori";
              
              $q = mysql_query($queryKat) or die (mysql_error());
			    
				if(mysql_num_rows($q) > 0){
				while($ftch = mysql_fetch_array($q)){
			
						echo "<input type=\"checkbox\"";
						echo " value=\"" .  $ftch['idkategori'] . "\"";
						echo " name=\"kategori[" . $ftch['idkategori'] . "]\"" ;
						echo " />" . $ftch['nama'] ;
						}
				}
			?>					
		    
		    <label for="file">File Buku:</label>
			<input type="file" name="file" id="file"><br>
            
            <input type="submit" value="Tambah">
		  </form>
		  <p>&nbsp;</p>
		</div>
        </div>
      </div>
      <!-- FIRST SORTABLE COLUMN END -->
      <!-- SECOND SORTABLE COLUMN START -->
      <div class="column">
      
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
        <div class="portlet-header fixed"><img src="images/icons/user.gif" width="16" height="16" alt="Tabel Buku" /> Tabel Buku</div>
		<div class="portlet-content nopadding">
        <form action="" method="post">
          <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
              <tr>
                
                <th width="136" scope="col">Cover</th>
                <th width="102" scope="col">Judul</th>
                <th width="109" scope="col">Pengarang</th>
                <th width="129" scope="col">Tahun</th>
                <th width="171" scope="col">Kategori</th>
                
                <th width="90" scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $querybuku = "select idbuku,cover,judul,pengarang,tahun,alamaturl from buku";
              
              $q = mysql_query($querybuku) or die (mysql_error());
			  $cover = "";
			  $judul = "";
			  $pengarang = "";
			  $tahun = "";
			  //$kategori = "";
			  $alamaturl = "";			  
				if(mysql_num_rows($q) > 0){
					while($ftch = mysql_fetch_array($q)){
					$cover=$ftch['cover'];
					$judul = $ftch['judul'];
					$pengarang = $ftch['pengarang'];
			  		$tahun = $ftch['tahun'];
			  		$alamaturl = $ftch['alamaturl'];
			  		$idbuku = $ftch['idbuku'];
			  		echo "<tr>";
			  		echo "<td><img src=\"" . $cover . "\" width=80 height=60 /></td>";
			  		echo "<td><a href=\"" . $alamaturl . "\">" . $judul . "</a></td>";
			  		echo "<td>" . $pengarang . "</td>";
			  		echo "<td>" . $tahun . "</td>";
			  		echo "<td>";
			  		$queryKategori = "SELECT k.nama FROM ebookkategori ek , kategori k WHERE ek.idbuku = $idbuku and k.idkategori = ek.idkategori";
			  		$query = mysql_query($queryKategori) or die (mysql_error());
					if(mysql_num_rows($query) > 0){
						while($ftch = mysql_fetch_array($query)){
							echo $ftch['nama'];
							echo ", ";
						}
					} 
					echo "</td>";
			  		?>
			  		<td></td>
			  		<td width="90"><a href="#" class="edit_icon" title="Edit"></a> </td>
              		</tr>
			  		<?php
					}
				}
				?>
              <tr>
                
                
                
              <tr class="footer">
                <td colspan="3"></td>
                <td align="right">&nbsp;</td>
                <td colspan="3" align="right">
				<!--  PAGINATION START  -->             
                    <div class="pagination">
                    <span class="previous-off">&laquo; Previous</span>
                    <span class="active">1</span>
                    <a href="query_41878854">2</a>
                    <a href="query_8A8058C2">3</a>
                    <a href="query_2823E521">4</a>
                    <a href="query_B322F5B7">5</a>
                    <a href="query_3A2A444D">6</a>
                    <a href="query_912D14DB">7</a>
                    <a href="query_41878854" class="next">Next &raquo;</a>
                    </div>  
                <!--  PAGINATION END  -->       
                </td>
              </tr>
            </tbody>
          </table>
        </form>
		</div>
      </div>
<!--  END #PORTLETS -->  
   </div>
    <div class="clear"> </div>
<!-- END CONTENT-->    
<?php include('footer.php');?>