<?php
include_once "koneksi/DB_Connect.php";
$db = new DB_Connect();
$db->connect();

include('header.php');
include('menuatas.php');
include('prosesindexing.php');
?>
<!-- CONTENT START -->
    <div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
    <h1 class="dashboard">Proses Buku</h1>
    </div>
    
    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    

    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
        <div class="portlet-header fixed"><img src="images/icons/user.gif" width="16" height="16" alt="Tabel Buku" /> Tabel Buku</div>
		<div class="portlet-content nopadding">
        <form action="" method="post">
          <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
              <tr>
                
                <th width="136" scope="col">Cover</th>
                <th width="102" scope="col">Judul</th>
                <th width="109" scope="col">Pengarang</th>
                <th width="60" scope="col">Tahun</th>
                <th width="240" scope="col">Proses</th>
                
                
              </tr>
            </thead>
            <tbody>
              <?php
              $indexing = new prosesindexing("prosesindex.php");
              $querybuku = "select idbuku,cover,judul,pengarang,tahun,alamaturl from buku";
              
              $q = mysql_query($querybuku) or die (mysql_error());
			  $cover = "";
			  $judul = "";
			  $pengarang = "";
			  $tahun = "";
			  //$kategori = "";
			  $alamaturl = "";			  
				if(mysql_num_rows($q) > 0){
					while($ftch = mysql_fetch_array($q)){
					$cover=$ftch['cover'];
					$judul = $ftch['judul'];
					$pengarang = $ftch['pengarang'];
			  		$tahun = $ftch['tahun'];
			  		$id = $ftch['idbuku'];
			  		$alamaturl = $ftch['alamaturl'];
			  		echo "<tr>";
			  		echo "<td><img src=\"" . $cover . "\" width=80 height=60 /><input type=hidden name=idbuku value=$id></td>";
			  		echo "<td><a href=\"" . $alamaturl . "\">" . $judul . "</a></td>";
			  		echo "<td>" . $pengarang . "</td>";
			  		echo "<td>" . $tahun . "</td>";
			  		//echo "<td>" . $kategori . "</td>";
			  		
			  		?>
			  		<td width="90"><a href="filteringtoken.php?buku=<?php echo $alamaturl ?>" title="Filtering & Tokenizing">Filtering & Token</a>-><a href="stemmingporter.php" title="Stemming Porter Bahasa">Stemming Porter</a>-><a href="termindexing.php?idbuku=<?php echo $id ; ?>" title="Terms">Ebook Term Indexing</a></td>
              		</tr>
			  		<?php
					}
				}
				?>
              <tr>
                
                
                
              <tr class="footer">
                <td colspan="3"></td>
                <td align="right">&nbsp;</td>
                <td colspan="3" align="right">
				<!--  PAGINATION START  -->             
                    <div class="pagination">
                    <span class="previous-off">&laquo; Previous</span>
                    <span class="active">1</span>
                    <a href="query_41878854">2</a>
                    <a href="query_8A8058C2">3</a>
                    <a href="query_2823E521">4</a>
                    <a href="query_B322F5B7">5</a>
                    <a href="query_3A2A444D">6</a>
                    <a href="query_912D14DB">7</a>
                    <a href="query_41878854" class="next">Next &raquo;</a>
                    </div>  
                <!--  PAGINATION END  -->       
                </td>
              </tr>
            </tbody>
          </table>
        </form>
		</div>
      </div>
<!--  END #PORTLETS -->  
   </div>
    <div class="clear"> </div>
<!-- END CONTENT-->    
<?php

include('footer.php');

if(ISSET($_GET['action'])){
	if($_GET['action'] == "ft"){
	$urlbuku = $_GET['buku'];
	$indexing->FilteringTokenizing('http://localhost/textmining/admin/$urlbuku');
	}
	if($_GET['action'] == "st"){
	$indexing->Stemming();
	}
	if($_GET['action'] == "ti"){
	$idb = $_GET['idb'];
	$indexing->TermIndexing($idb);
	}
}
?>