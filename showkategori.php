<!DOCTYPE html>
<html lang="pl">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TEXT MINING</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="bigbutton.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="favicon.png" />
	<link rel="canonical" href="index.html" />
	<link rel="stylesheet" type="text/css" media="all" href="public/css/nobo.css" />
        	
	<script type="text/javascript" src="public/js/modernizr.custom.88490.js"></script>
	<script type="text/javascript" src="public/js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="public/js/jquery-ui-1.9.1.custom.min.js"></script>
	<script type="text/javascript" src="public/js/whcookies.js"></script>
	<script src="../code.jquery.com/jquery-latest.min.js"></script>
	<script src="../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >
	<script type="text/javascript" src="public/js/corrects.js"></script>
</head>

<body>
<?php
include "koneksi/DB_Connect.php";
$db = new DB_Connect();
$db->connect();
?>
		
<style>
	/* CSS Tree menu styles */
ol.tree
{
	padding: 0 0 0 0px;
	width: 300px;
}
	li 
	{ 
		position: relative; 
		list-style: none;
	}
	li.file
	{
		margin-left: -1px !important;
	}
		li.file a
		{
			background: url(img/document.png) 0 0 no-repeat;
			color: #000;
			padding-left: 21px;
			text-color:#000;
			text-decoration: none;
			display: block;
		}
		li.file a[href *= '.pdf']	{ background: url(img/document.png) 0 0 no-repeat; }
		li.file a[href *= '.html']	{ background: url(img/document.png) 0 0 no-repeat; }
		li.file a[href $= '.css']	{ background: url(img/document.png) 0 0 no-repeat; }
		li.file a[href $= '.js']		{ background: url(img/document.png) 0 0 no-repeat; }
	li input
	{
		position: absolute;
		left: 0;
		margin-left: 0;
		opacity: 0;
		z-index: 2;
		cursor: pointer;
		height: 1em;
		width: 1em;
		top: 0;
	}
		li input + ol
		{
			background: url(img/toggle-small-expand.png) 40px 0 no-repeat;
			margin: -0.938em 0 0 -44px; /* 15px */
			height: 1em;
		}
		li input + ol > li { display: none; margin-left: -14px !important; padding-left: 1px; }
	li label
	{
		background: url(img/folder-horizontal.png) 15px 1px no-repeat;
		cursor: pointer;
		display: block;
		padding-left: 37px;
		color:#000000;
	}

	li input:checked + ol
	{
		background: url(img/toggle-small.png) 40px 5px no-repeat;
		margin: -1.25em 0 0 -44px; /* 20px */
		padding: 1.563em 0 0 80px;
		height: auto;
	}
		li input:checked + ol > li { display: block; margin: 0 0 0.125em;  /* 2px */}
		li input:checked + ol > li:last-child { margin: 0 0 0.063em; /* 1px */ }
		</style>
		<script>
function getMateri(id)
{
if (id=="")
  {
  document.getElementById("board").innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("detilMateri").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","getmateri.php?idmateri="+id ,true);
xmlhttp.send();
}
		</script>
    <ol class="tree">
<?php
function display_children($parent, $level) { 

    // retrieve all children of $parent
    $result = mysql_query('SELECT idkategori id_mapel,nama nama_mapel FROM kategori WHERE idparent="'.$parent.'";');
 
    // display each child
    while ($row = mysql_fetch_array($result)) {  
	?>
		<li><label for="folder<?php echo $row['id_mapel'];?>">
        <?php
		// indent and display the title of this child 
        //echo str_repeat('  ',$level).$row['nama_mapel']."\n"; //<br>  
		echo $row['nama_mapel']."\n";
		?>
		</label><input type="checkbox" name="id" id="folder<?php echo $row['id_mapel'];?>" onClick="getMateri(<?php echo $row['id_mapel'];?>)"/>

		<ol>
		<?
        display_children($row['id_mapel'], $level+1);
?>		
		</li></ol>
<?php   }  ?>
	</li>
<?php }  
echo "<br>" . display_children(0,0);
?>
</ol>
<div id="detilMateri"
	style="position:absolute;left:340px; top: 10px; width: 540px; height:600px; z-index: 3;overflow:auto;color:#000000;">
</div>	

</body>
</html>