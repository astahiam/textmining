<?php
include 'admin/Classes/stem_indonesia.php';

include "koneksi/DB_Connect.php";
$db = new DB_Connect();
$db->connect();

class Preprocessing
{

function tokenizing($cari){

$awal = microtime(true);
$clearPdf = mysql_query("truncate table pdf") or die (mysql_error());

$cari = $_GET["search"];	

 $insertTEMP = "";
 $jumlahKata = 0;
 $jumlahHalaman = sizeof($texts);
 

//hilangkan tanda baca cth: , . " ; _ - ' ! ? { }  atau selain huruf , ",", "." , "!", "?" , ":" , ";"
$cari = str_replace(",", "", $cari );
$cari  = str_replace(".", "", $cari );
$cari  = str_replace(";", "", $cari ); 
$cari  = str_replace("(", "", $cari ); 
$cari  = str_replace(")", "", $cari ); 
$cari  = str_replace(" : ", " ", $cari );
$cari  = str_replace(":", "", $cari );
$cari  = str_replace("…", "", $cari );
//$cari  = str_replace("-", "", $cari );
$cari  = str_replace("?", "", $cari );
$cari  = str_replace("\"", "", $cari );
$cari  = str_replace("+", "", $cari );
$cari  = str_replace("=", "", $cari );
$cari  = str_replace("%", "", $cari );
$cari = str_replace("\n", " ", $cari );
$cari = str_replace("***", "",$cari );
$cari = str_replace("‘", "",$cari);
$cari = str_replace("’", "", $cari);
$cari = str_replace("”", "", $cari);
$cari = str_replace("“","", $cari);
$cari = str_replace("!","", $cari);
$cari = str_replace("'","", $cari);
//$cari = str_replace("www", "", $cari);
//$cari = str_replace("wwwrajaebookgratiscom", "", $textx[$x]);

//membuat lowercase (huruf kecil semua)
$cari = strtolower($cari);
	//echo $cari;
return $terms = explode(" ",$cari);
//$k = 1;
}

function insertToken($terms){

$headInsert = "INSERT INTO PDF (nama, detil) VALUES ";
$tempInsert = "";

$stringData = "";
$tempInsert = "";
$j = 0;
foreach ($terms as $t) {
if(strlen($t) > 2){
$jumlahKata += 1;
$stringData .= $jumlahKata . " : " . ltrim(rtrim($t)) . " : " . "1" . "\n";
$tempInsert .= "('" . ltrim(rtrim($t)) . "','1'),";
	}
	$j=$j+1;
}
$tempInsert = substr($tempInsert,0,-1);
	$tempInsert .= ";";
	//echo $tempInsert;
	
	$query = $headInsert . $tempInsert;
	
	mysql_query($query) or die (mysql_error());
	
	
unset($terms);

echo "jumlah Kata " . $jumlahKata; 
echo " jumlah Halaman " . $jumlahHalaman; 
echo "<br>";
}
 
//remove stopwords
function removeStopwords(){
 	$stopwordsremover = "delete from pdf where nama in (select kata from stopwords)";
 	$jumlahterhapus = mysql_query($stopwordsremover) or die (mysql_error());
 	
}
//step 2
function stemming()
 	try {
    $idx = 0;
    
    $queryTemp = mysql_query("select * from pdf") or die (mysql_error());
    $jumlahKata = mysql_num_rows($queryTemp);
    //echo $jumlahKata;
 	if (mysql_num_rows($queryTemp) > 0)
 	{
 		$terms = array();
 		$idx2 = 1;
 		//stemming porter bahasa
 		while($row = mysql_fetch_array($queryTemp) ){
 			$dt = $row['nama'];
 			
			if (strlen($dt) > 2 ){
				//echo  $idx2 . " " . $dt . " :";
 				$terms[$idx] = $dt;
 				$idx = $idx + 1;
 				//stemming porter bahasa indonesia
 				$word = $dt;
				$si = new stem_indonesia($word);
				$dtstm = $si->dasar;
 				//echo $dtstm . "|";
 				$insertStem = mysql_query("INSERT INTO pdf SET  nama = '" . $dtstm . "', detil = '2'");
 				$idx2 = $idx2 +1;

 				}
 			}
 		} else {echo "query gagal";}
 		
	
	} catch(Exception $e) {
		$message = $e->getMessage();
	}
}

function getQuestion(){
//step 3
$termQuestion = array();
try {
    $idbuku = $_GET['idbuku'];  
    $queryTemp = mysql_query("SELECT nama term, COUNT( * ) jumlah FROM pdf WHERE detil = 2 AND nama NOT IN ('') GROUP BY nama") or die (mysql_error());
    $jumlahKata = mysql_num_rows($queryTemp);
    //echo $jumlahKata;
 	if (mysql_num_rows($queryTemp) > 0)
 	{
 		
 		$idx2 = 0;
 		//stemming porter bahasa
 		while($row = mysql_fetch_array($queryTemp) ){
 			$record = array();
 			$record['term'] = $row['term'];
 			$record['jumlah'] = $row['jumlah'];

 			array_push($termQuestion, $record); 

 			}
 		} else {echo "query gagal";}
 		
	
	} catch(Exception $e) {
		$message = $e->getMessage();
	}
	
 	
$akhir = microtime(true);
$lama = $akhir - $awal ;
	return $termQuestion;
}
//cari TF
//ambil dari table ebookterm;
//hitung jumlah document
//$jd = 0;
function getJumlahDokumen(){
$q = "select count(distinct(idbuku)) jd from ebookterm";
$select = mysql_query($q) or die (mysql_error());
					$jd=0;
					while($ftch = mysql_fetch_array($select)){
					$jd=$ftch['jd'];
					}					
					return $jd;
}

//tf * IDF
function pembobotanKataKunci($jd, $termQuestion){
	//tf
	$qtf = "SELECT e.*, t.term FROM ebookterm e, terms t where e.idterms = t.idterms";
	$selectTF = mysql_query($qtf) or die (mysql_error());
	$tf = array();

	while($ftch = mysql_fetch_array($selectTF)){
					$record = array();
					$record['idebookterm']=$ftch['idebookterm'];
					$record['idbuku']=$ftch['idbuku'];
					$record['idterms']=$ftch['idterms'];
					$record['jumlah']=$ftch['jumlah'];
					$record['term']=$ftch['term'];
        			array_push($tf, $record); 
					}
		echo "jumlah row tf: " . mysql_num_rows($selectTF);				
	echo "jumlah arr tf: " . sizeof($tf);
	echo "<br>";
	//tf question
	
	
	//DF
	$qtf = "SELECT * FROM  terms t";
	$selectDF = mysql_query($qtf) or die (mysql_error());
	$DF = array();
	
	while($ftch = mysql_fetch_array($selectDF)){
					$record = array();

					$record['idterms']=$ftch['idterms'];
					$record['jumlah']=$ftch['jumlah'];
					$record['term']=$ftch['term'];
					array_push($DF, $record); 

					}
		echo "jumlah arr df: " . sizeof($DF);
	echo "<br>";

	$IDF = array();
	
	foreach($DF as $t){
		$record = array();

		$record['idterms'] = $t['idterms'];
		$record['term']=$t['term'];
		$nilaiIDF = $jd/$t['jumlah'];
		//echo "niai IDF: " . $nilaiIDF;
		$record['jumlah']=log($nilaiIDF, 10);
		//echo "<br>setelah log: " . log($nilaiIDF, 10);
		//echo "<br>";
		array_push($IDF, $record); 
			
	}
		echo "jumlah arr IDF: " . sizeof($IDF);
	echo "<br>";

	$bobot = array();
	$dokumenBobot = array();
	$questionBobot = array();
	
	foreach($tf as $ftch){
					$nilaiIDF = 0;
					$record = array();

					foreach($IDF as $nidf){
						if($nidf['term'] == $ftch['term']){
							$nilaiIDF = $nidf['jumlah'];
						}
					}
					$record['idebookterm']=$ftch['idebookterm'];
					$record['idbuku']=$ftch['idbuku'];
					$record['idterms']=$ftch['idterms'];
					$record['jumlah']=$ftch['jumlah'] * $nilaiIDF;
					$record['term']=$ftch['term'];
					//echo "tf IDF: " . $record['term'] . " " . $record['jumlah'];
					//echo "<br>";
					array_push($bobot, $record); 
					array_push($dokumenBobot, $record); 
					}
					
	foreach($termQuestion as $qu){
					$nilaiIDF = 0;
					$record = array();
					
					foreach($IDF as $nidf){
						if($nidf['term'] == $qu['term']){
							//echo "set q bobot: " . $nidf['term'] . " = " . $nidf['jumlah'] . "<br>";
							$nilaiIDF = $nidf['jumlah'];
						}
					}
					$record['idebookterm']=0;
					$record['idbuku']=0;
					$record['idterms']=$ftch['idterms'];
					
					//$qu['jumlah']=$qu['jumlah'] * $nilaiIDF;
					$record['jumlah']=$qu['jumlah'] * $nilaiIDF;
					$record['term']=$qu['term'];
					//echo "question " . $qu['term'] . " " . $qu['jumlah'];
					//echo "<br>";
					array_push($bobot, $record);
					array_push($questionBobot, $record);  
	}
	return $bobot;
}

}