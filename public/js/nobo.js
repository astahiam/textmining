$(document).ready
(
	function()
	{
		// Szerokosc menu
		$(window).trigger('resize');

		// Obsluga linkowania
		$('a[href^="/#"]').click
		(
			'live',
			function(e)
			{
				e.preventDefault();
				//---
				var that = $(this).attr('href');
				if (that.substr(0, 1) == '/') that = that.substr(1);
				if (that.length > 1)
				{
					if (that == 'portfolio')
					{
						$(".down-arrow").trigger('click');
					}
					else
					{
						$('html, body').animate
						(
							{
								scrollTop: Math.min
								(
									$(that).offset().top - $('.menu').height() + 4,
									$(that).offset().top
								)
							},
							1000,
							'swing'
						);
					}
				}
			}
		);

		// Preload obrazkow
		$.fn.preload = function()
		{
			this.each
			(
				function()
				{
					$('<img/>')[0].src = this;
				}
			);
		}
		$([
			'public/gfx/desktop/zobacz.png',
			'public/gfx/desktop/zlap.png',
			'public/gfx/desktop/email.png',
			'public/gfx/desktop/phone.png',
			'public/gfx/desktop/lewo.png',
			'public/gfx/desktop/prawo.png',
			'public/gfx/desktop/menu.png',
			'public/gfx/desktop/nobo.png',
			'public/gfx/desktop/separator.png',
			'public/gfx/desktop/down_arrow.png',
			'public/gfx/desktop/tlo.png',
			'public/gfx/desktop/submit.png',
			'public/gfx/desktop/zobacz-projekt.png',
			'public/gfx/desktop/page1.png',
			'public/gfx/desktop/page2.png',
			'public/gfx/desktop/page3.png',
			'public/gfx/desktop/page4.png',
			'public/gfx/desktop/footer.png',
			'public/gfx/desktop/nav.png',
			'public/gfx/desktop/analiza.png',
			'public/gfx/desktop/kreacja.png',
			'public/gfx/desktop/strategia.png',
			'public/gfx/desktop/bullet.png'
		]).preload();

		// Listy
		$('.menu .links li:first, .menu .langs li:first').addClass('first')

		// Animowany hover menu
		$('.menu .links li a, .menu .langs li a').hover
		(
			function()
			{
				$(this).stop().animate
				(
					{
						'color': '#15D8ED'
					},
					'fast'
				);
			},
			function()
			{
				$(this).stop().animate
				(
					{
						'color': '#1B1B1B'
					},
					'fast'
				);
			}
		);

		// Portfolio
		$(".portfolio-content ul li").hover
		(
			function()
			{
				$(this).addClass('active');
			},
			function()
			{
				$(this).removeClass('active');
			}
		);

		var PortfolioContentWidth = 0;
		$(".portfolio-content ul li").each
		(
			function()
			{
				PortfolioContentWidth += $(this).width();
			}
		);
		$(".portfolio-content").width(PortfolioContentWidth);

		$(".portfolio .lewo").click
		(
			function(e)
			{
				e.preventDefault();

				var Wrapper = $(".portfolio");
				var WrapperWidth = Wrapper.width();
				var Step = 438;
				var Container = $(".portfolio-content");
				var ContainerWidth = Container.width();
				var Left = parseInt(Container.css("left")) || 0;
				var AbsLeft = Math.abs(Left);

				if (AbsLeft < Step) Step = AbsLeft;
				Container.stop(true).animate( { "left": "+=" + Step + "px" }, "slow", function() { } );
			}
		);

		$(".portfolio .prawo").click
		(
			function(e)
			{
				e.preventDefault();

				var Wrapper = $(".portfolio");
				var WrapperWidth = Wrapper.width();
				var Step = 438;
				var Container = $(".portfolio-content");
				var ContainerWidth = Container.width();
				var Left = parseInt(Container.css("left")) || 0;
				var AbsLeft = Math.abs(Left);

				if (ContainerWidth - AbsLeft - WrapperWidth > Step)
				{
					Container.stop(true).animate( { "left": "-=" + Step + "px" }, "slow", function() { } );
				}
				else
				{
					Container.stop(true).animate( { "left": "-=" + (ContainerWidth - AbsLeft - WrapperWidth) + "px" }, "slow", function() { } );
				}
			}
		);

		// Down arrow
		$(".down-arrow").click
		(
			function(e)
			{
				e.preventDefault();
				if ($(this).hasClass('opened'))
				{
					$(".portfolio").stop().animate
					(
						{
							'marginTop': '-292'
						},
						'fast'
					);
					$(".portfolio .lewo, .portfolio .prawo").stop().animate
					(
						{
							'top': '-240'
						},
						'fast'
					);
					$(".menu").stop().animate
					(
						{
							'top': '0'
						},
						'fast'
					);

					$(this).removeClass('opened');
					$(this).addClass('closed');
				}
				else
				{
					$(".portfolio").stop().animate
					(
						{
							'marginTop': '0'
						},
						'fast'
					);
					$(".portfolio .lewo, .portfolio .prawo").stop().animate
					(
						{
							'top': '52'
						},
						'fast'
					);
					$(".menu").stop().animate
					(
						{
							'top': '292'
						},
						'fast'
					);

					$(this).removeClass('closed');
					$(this).addClass('opened');
				}
			}
		);

		$(".trigger-portfolio").click
		(
			function(e)
			{
				e.preventDefault();
				$(".down-arrow").trigger('click');
			}
		);

		// Koolko nawigacyjne
		$('.analiza-link, .kreacja-link, .strategia-link').click
		(
			function(e)
			{
				e.preventDefault();
				var Section = $(this).attr('class').split('-')[0];
				//---
				$('.analiza-submenu, .kreacja-submenu, .strategia-submenu').hide();
				$('.analiza-content, .kreacja-content, .strategia-content').hide();
				$('.circle').removeClass('analiza').removeClass('kreacja').removeClass('strategia');
				//---
				$('.' + Section + '-submenu').show();
				$('.' + Section + '-submenu-link-0').addClass('active');
				$('.' + Section + '-content-0').show();
				$('.circle').addClass(Section);
			}
		);

		$('.analiza-submenu-link, .kreacja-submenu-link, .strategia-submenu-link').click
		(
			function(e)
			{
				e.preventDefault();
				var Section = $(this).attr('class').split(' ')[1].split('-')[0];
				var SubSection = $(this).attr('class').split(' ')[1].split('-')[3];
				//---
				$('.analiza-submenu-link, .kreacja-submenu-link, .strategia-submenu-link').removeClass('active');
				$(this).addClass('active');
				//---
				$('.analiza-content, .kreacja-content, .strategia-content').hide();
				$('.' + Section + '-content-' + SubSection).show();
			}
		);

		$('.analiza-link').trigger('click');

		// Formularz
		$(".submitform").click(function()
		{
			$("#hash").val("8741631d447308de69eddcbf43b04cc3c0987a2c");

			formName = $(".formName").val();
			formEmail = $(".formEmail").val();
			formTel = $(".formTel").val();
			formContent = $(".formContent").val();

			if (formName == 'imię i nazwisko' || formEmail == 'email' || formTel == 'numer telefonu' || formContent == 'w czym możemy pomóc ?')
			{
				alert("Wypełnij wszystkie pola.");
				return false;
			}

			if (formName == '' || formEmail == '' || formContent == '' || formTel == '')
			{
				alert("Wypełnij wszystkie pola.");
				return false;
			}

			$("#contactForm").submit();
		});

		// Zobacz
		$('.zobacz').hover
		(
			function() { $(this).stop().animate({'marginLeft': '-240'}, 'slow', function() { }); },
			function() { $(this).stop().animate({'marginLeft': '-100'}, 'slow', function() { }); }
		);

		// Left, right, vline
		var Max = Math.max($(".left").height(), $(".right").height());
		$(".left, .right").height(Max);
//		$(".vline").height(Max - 35);
		$(".vline").height(Max);

		// Zlap
		$('.zlap').hover
		(
			function() { $(this).stop().animate({'marginLeft': '-219'}, 'slow', function() { }); },
			function() { $(this).stop().animate({'marginLeft': '-70'}, 'slow', function() { }); }
		);

		// Zobacz online
		$(".zobacz-online").hover
		(
			function() { $(this).find('div').stop().animate({'marginLeft': '0'}, 'fast', function() { }); },
			function() { $(this).find('div').stop().animate({'marginLeft': '-20'}, 'fast', function() { }); }
		);

		// Przed / Po
		$('.Before').hover
		(
			function() { $(this).fadeOut('fast', function(){ $('.After').fadeIn('fast'); }); },
			function() {  }
		);

		$('.After').hover
		(
			function() { },
			function() { $(this).fadeOut('fast', function(){ $('.Before').fadeIn('fast'); }); }
		);
	}
);

$(window).resize
(
	function()
	{
		$('.menu').css('width', Math.max('1200', $(window).width() - parseInt($('.menu').css('padding-left')) - parseInt($('.menu').css('padding-right'))));
		$('.portfolio-page').css('min-height', $(window).height());
	}
);