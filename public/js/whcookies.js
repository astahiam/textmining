function getCookie(name){
  var str = '; '+ document.cookie +';';
  var index = str.indexOf('; '+ escape(name) +'=');
  if (index != -1) {
    index += name.length+3;
    var value = str.slice(index, str.indexOf(';', index));
    return unescape(value);
  }
};
function setCookie(name, value, expires) {
  var cookieStr = escape(name) +"=";
  if (typeof value != "undefined") {
    cookieStr += escape(value);
  }
  if (!expires) {
    expires = new Date();
    expires.setTime(expires.getTime()+366*24*60*60*1000);
  }
  cookieStr += "; expires="+ expires.toGMTString() +";";
  document.cookie = cookieStr;
};
function deleteCookie(name){
  var past = new Date();
  past.setTime(0); // 1970-01-01
  setCookie(name, null, past);
};

function CookiesOK() { setCookie("cinfo", "1");document.getElementById("cookies_content").style.display="none"; }