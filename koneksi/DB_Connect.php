<?php
class DB_Connect {

 	// method buka koneksi ke database
    public function connect() {
        require_once 'koneksi/config.php';
        // koneksi mysql
        $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
        // select database
        mysql_select_db(DB_DATABASE);
        
        return $con;
    }

    // method menutup koneksi
    public function close() {
        mysql_close();
    }

}

?>
